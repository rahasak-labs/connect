package com.rahasak.connect.protocol

import spray.json.{DefaultJsonProtocol, JsValue, RootJsonFormat}

case class Pagination(offset: Int, count: Int, nextPage: Option[String])

object PaginationProtocol extends DefaultJsonProtocol {

  implicit object HeaderFormat extends RootJsonFormat[Pagination] {
    def write(obj: Pagination) = ???

    def read(json: JsValue): Pagination = {
      val fields = json.asJsObject.fields
      Pagination(
        fields("offset").convertTo[Int],
        fields("count").convertTo[Int],
        fields("next_page").convertTo[Option[String]]
      )
    }
  }

}

case class SearchHit(id: String, name: Option[String], pageContent: Option[String])

object SearchHitProtocol extends DefaultJsonProtocol {

  implicit object SearchHitFormat extends RootJsonFormat[SearchHit] {
    def write(obj: SearchHit) = ???

    def read(json: JsValue): SearchHit = {
      val fields = json.asJsObject.fields
      SearchHit(
        fields("id").convertTo[String],
        fields("name").convertTo[Option[String]],
        fields.get("page_content").map(_.convertTo[String])
      )
    }
  }

}

case class Result(pagination: Pagination, searchHits: List[SearchHit])

object ResultProtocol extends DefaultJsonProtocol {

  import PaginationProtocol._
  import SearchHitProtocol._

  implicit object ResultFormat extends RootJsonFormat[Result] {
    def write(obj: Result) = ???

    def read(json: JsValue): Result = {
      val fields = json.asJsObject.fields
      Result(
        fields("pagination").convertTo[Pagination],
        fields("search_hits").convertTo[List[SearchHit]]
      )
    }
  }

}
