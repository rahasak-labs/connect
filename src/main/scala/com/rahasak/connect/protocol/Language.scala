package com.rahasak.connect.protocol

case class Language(id: String, name: String, isFunctional: Boolean)
